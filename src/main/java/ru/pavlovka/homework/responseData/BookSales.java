package ru.pavlovka.homework.responseData;

public interface BookSales {
    String getBook();
    String getAreaname();
    double getSalesum();
    int getBookcount();
}
