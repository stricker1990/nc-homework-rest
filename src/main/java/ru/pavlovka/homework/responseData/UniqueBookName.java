package ru.pavlovka.homework.responseData;

public interface UniqueBookName {
    String getName();
    Double getCost();
}
