package ru.pavlovka.homework.responseData;

public interface CustomersOnlyNameAndDiscount {
    String getName();
    double getDiscount();
}
