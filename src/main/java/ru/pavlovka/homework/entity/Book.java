package ru.pavlovka.homework.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="name")
    private String name;

    @Column(name = "cost")
    private double cost;

    @Column(name = "count")
    private int count;

    @OneToOne
    private Shop shop;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                Double.compare(book.cost, cost) == 0 &&
                count == book.count &&
                Objects.equals(name, book.name) &&
                Objects.equals(shop, book.shop);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cost, count, shop);
    }
}
