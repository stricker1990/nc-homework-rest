package ru.pavlovka.homework.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="shop")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToOne
    private Area area;

    @Column(name = "discount")
    private double discount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shop shop = (Shop) o;
        return id == shop.id &&
                Double.compare(shop.discount, discount) == 0 &&
                Objects.equals(name, shop.name) &&
                Objects.equals(area, shop.area);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, area, discount);
    }
}
