package ru.pavlovka.homework.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name="sale")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @OneToOne
    @JoinColumn(name = "seller")
    private Shop seller;

    @OneToOne
    @JoinColumn(name = "customer")
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "book")
    private Book book;

    @Column(name = "count")
    private int count;

    @Column(name = "sum")
    private double sum;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Shop getSeller() {
        return seller;
    }

    public void setSeller(Shop seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sale sale = (Sale) o;
        return id == sale.id &&
                count == sale.count &&
                Double.compare(sale.sum, sum) == 0 &&
                Objects.equals(date, sale.date) &&
                Objects.equals(seller, sale.seller) &&
                Objects.equals(customer, sale.customer) &&
                Objects.equals(book, sale.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, seller, customer, book, count, sum);
    }
}
