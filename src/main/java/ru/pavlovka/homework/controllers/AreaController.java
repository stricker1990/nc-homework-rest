package ru.pavlovka.homework.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.exceptions.NotFoundException;
import ru.pavlovka.homework.repository.AreaRepository;
import ru.pavlovka.homework.responseData.CustomersOnlyNameAndDiscount;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/area")
public class AreaController {
    @Autowired
    private AreaRepository areaRepository;

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteArea(@PathVariable int id){
        areaRepository.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(code= HttpStatus.CREATED)
    public void createArea(@RequestBody Area area){
        areaRepository.save(area);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Area> patchArea(@PathVariable int id, @RequestBody JsonPatch jsonPatch){
        try{
            Area area = areaRepository.findById(id).orElseThrow(NotFoundException::new);
            Area areaPatched = applyPatchToArea(jsonPatch, area);
            areaRepository.save(areaPatched);
            return ResponseEntity.ok(areaPatched);
        }catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Area> updateArea(@RequestBody Area area) {
        try {
            Area foundArea = areaRepository.findById(area.getId()).orElseThrow(NotFoundException::new);
            areaRepository.save(area);
            return ResponseEntity.ok(area);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/{id}")
    public ResponseEntity<Area> getArea (@PathVariable int id) {
        try {
            return ResponseEntity.ok(areaRepository.findById(id).orElseThrow(NotFoundException::new));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/")
    public ResponseEntity<Collection<Area>> getAllArea(){
        return ResponseEntity.ok(areaRepository.findAll());
    }

    @GetMapping("/{id}/customers")
    public ResponseEntity<Collection<CustomersOnlyNameAndDiscount>> getCustomersByArea(@PathVariable int id){
        return ResponseEntity.ok(areaRepository.getCustomersByArea(id));
    }

    @GetMapping("/{id}/shops")
    public  ResponseEntity<Collection<String>> getShopsByArea(@PathVariable int id){
        return ResponseEntity.ok(areaRepository.getShopsByArea(id));
    }

    private Area applyPatchToArea(JsonPatch patch, Area area) throws JsonPatchException, JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();

        JsonNode patched = patch.apply(objectMapper.convertValue(area, JsonNode.class));
        return objectMapper.treeToValue(patched, Area.class);
    }
}
