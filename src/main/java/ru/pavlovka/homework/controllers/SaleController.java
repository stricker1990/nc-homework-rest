package ru.pavlovka.homework.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.entity.Customer;
import ru.pavlovka.homework.entity.Sale;
import ru.pavlovka.homework.exceptions.NotFoundException;
import ru.pavlovka.homework.repository.AreaRepository;
import ru.pavlovka.homework.repository.CustomerRepository;
import ru.pavlovka.homework.repository.SaleRepository;
import ru.pavlovka.homework.responseData.BookSales;

import javax.websocket.server.PathParam;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sale")
public class SaleController {
    @Autowired
    private SaleRepository repository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AreaRepository areaRepository;

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@PathVariable int id){
        repository.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public void create(@RequestBody Sale entity){
        repository.save(entity);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Sale> patch(@PathVariable int id, @RequestBody JsonPatch jsonPatch){
        try{
            Sale entity = repository.findById(id).orElseThrow(NotFoundException::new);
            Sale entityPatched = applyPatch(jsonPatch, entity);
            repository.save(entityPatched);
            return ResponseEntity.ok(entityPatched);
        }catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Sale> update(@RequestBody Sale request) {
        try {
            Sale foundShop = repository.findById(request.getId()).orElseThrow(NotFoundException::new);
            repository.save(request);
            return ResponseEntity.ok(request);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/{id}")
    public ResponseEntity<Sale> get (@PathVariable int id) {
        try {
            return ResponseEntity.ok(repository.findById(id).orElseThrow(NotFoundException::new));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/")
    public ResponseEntity<Collection<Sale>> getAll(@RequestParam Optional<Double> minSum){
        if(!minSum.isPresent()) {
            return ResponseEntity.ok(repository.findAll());
        }
        return ResponseEntity.ok(repository.getAllBy(minSum.get()));
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Collection<Sale>> getAllByCustomer(@PathVariable int id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        Customer customer = null;
        try {
            customer = customerRepository.findById(id).orElseThrow(NotFoundException::new);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        int areaId=customer.getId();
        return ResponseEntity.ok(repository.getAllBySellerAndArea(id, Optional.of(date).orElse(new Date()), areaId));
    }

    @GetMapping("/area/{id}")
    public ResponseEntity<Collection<BookSales>> getSalesByArea(@PathVariable int id, @RequestParam Optional<Integer> minCount){
        try {
            Area area=areaRepository.findById(id).orElseThrow(NotFoundException::new);
            return ResponseEntity.ok(repository.getBookSales(area.getName(), id, minCount.orElse(Integer.MIN_VALUE)));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/months")
    public ResponseEntity<Collection<Date>> getMonths(){
        return ResponseEntity.ok(repository.getSaleMonths());
    }

    private Sale applyPatch(JsonPatch patch, Sale entity) throws JsonPatchException, JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();

        JsonNode patched = patch.apply(objectMapper.convertValue(entity, JsonNode.class));
        return objectMapper.treeToValue(patched, Sale.class);
    }
}
