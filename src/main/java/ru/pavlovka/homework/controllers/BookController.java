package ru.pavlovka.homework.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pavlovka.homework.entity.Book;
import ru.pavlovka.homework.exceptions.NotFoundException;
import ru.pavlovka.homework.repository.BookRepository;
import ru.pavlovka.homework.responseData.UniqueBookName;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookRepository repository;

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@PathVariable int id){
        repository.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public void create(@RequestBody Book entity){
        repository.save(entity);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Book> patch(@PathVariable int id, @RequestBody JsonPatch jsonPatch){
        try{
            Book entity = repository.findById(id).orElseThrow(NotFoundException::new);
            Book entityPatched = applyPatchToBook(jsonPatch, entity);
            repository.save(entityPatched);
            return ResponseEntity.ok(entityPatched);
        }catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Book> update(@RequestBody Book request) {
        try {
            Book foundShop = repository.findById(request.getId()).orElseThrow(NotFoundException::new);
            repository.save(request);
            return ResponseEntity.ok(request);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/{id}")
    public ResponseEntity<Book> get (@PathVariable int id) {
        try {
            return ResponseEntity.ok(repository.findById(id).orElseThrow(NotFoundException::new));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/")
    public ResponseEntity<Collection<Book>> getAll(@RequestParam Optional<String> keyword, @RequestParam Optional<Double> minCost){
        if(!keyword.isPresent() && !minCost.isPresent()) {
            return ResponseEntity.ok(repository.findAll());
        }
        return  ResponseEntity.ok(repository.getAllBy(keyword.orElse(""), minCost.orElse(0.0d)));
    }

    @GetMapping("/price")
    public ResponseEntity<Collection<UniqueBookName>> getPrice(){
        return ResponseEntity.ok(repository.getUniqueNames());
    }

    private Book applyPatchToBook(JsonPatch patch, Book shop) throws JsonPatchException, JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();

        JsonNode patched = patch.apply(objectMapper.convertValue(shop, JsonNode.class));
        return objectMapper.treeToValue(patched, Book.class);
    }
}
