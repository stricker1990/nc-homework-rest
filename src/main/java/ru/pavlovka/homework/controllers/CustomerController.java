package ru.pavlovka.homework.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.entity.Customer;
import ru.pavlovka.homework.exceptions.NotFoundException;
import ru.pavlovka.homework.repository.AreaRepository;
import ru.pavlovka.homework.repository.CustomerRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerRepository repository;

    @Autowired
    private AreaRepository areaRepository;

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void delete(@PathVariable int id){
        repository.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public void create(@RequestBody Customer entity){
        repository.save(entity);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Customer> patch(@PathVariable int id, @RequestBody JsonPatch jsonPatch){
        try{
            Customer entity = repository.findById(id).orElseThrow(NotFoundException::new);
            Customer entityPatched = applyPatch(jsonPatch, entity);
            repository.save(entityPatched);
            return ResponseEntity.ok(entityPatched);
        }catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Customer> update(@RequestBody Customer request) {
        try {
            Customer foundShop = repository.findById(request.getId()).orElseThrow(NotFoundException::new);
            repository.save(request);
            return ResponseEntity.ok(request);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/{id}")
    public ResponseEntity<Customer> get (@PathVariable int id) {
        try {
            return ResponseEntity.ok(repository.findById(id).orElseThrow(NotFoundException::new));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/")
    public ResponseEntity<List<Customer>> getAll(){
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping("/areas")
    public ResponseEntity<Collection<Area>> getArea() {
        return ResponseEntity.ok(areaRepository.customerAreas());
    }

    private Customer applyPatch(JsonPatch patch, Customer entity) throws JsonPatchException, JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();

        JsonNode patched = patch.apply(objectMapper.convertValue(entity, JsonNode.class));
        return objectMapper.treeToValue(patched, Customer.class);
    }
}
