package ru.pavlovka.homework.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.entity.Shop;
import ru.pavlovka.homework.exceptions.NotFoundException;
import ru.pavlovka.homework.repository.ShopRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shop")
public class ShopController {
    @Autowired
    private ShopRepository shopRepository;

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteShop(@PathVariable int id){
        shopRepository.deleteById(id);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public void createShop(@RequestBody Shop shop){
        shopRepository.save(shop);
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Shop> patchShop(@PathVariable int id, @RequestBody JsonPatch jsonPatch){
        try{
            Shop shop = shopRepository.findById(id).orElseThrow(NotFoundException::new);
            Shop shopPatched = applyPatchToArea(jsonPatch, shop);
            shopRepository.save(shopPatched);
            return ResponseEntity.ok(shopPatched);
        }catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PutMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Shop> update(@RequestBody Shop shop) {
        try {
            Shop foundShop = shopRepository.findById(shop.getId()).orElseThrow(NotFoundException::new);
            shopRepository.save(shop);
            return ResponseEntity.ok(shop);
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/{id}")
    public ResponseEntity<Shop> get (@PathVariable int id) {
        try {
            return ResponseEntity.ok(shopRepository.findById(id).orElseThrow(NotFoundException::new));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping ("/")
    public ResponseEntity<Collection<Shop>> getAll(@RequestParam Optional<Integer> excludedArea, @RequestParam Optional<Double> minDiscount, @RequestParam Optional<Double> maxDiscount){
        if(!excludedArea.isPresent() && !minDiscount.isPresent() && !maxDiscount.isPresent()){
            return ResponseEntity.ok(shopRepository.findAll());
        }
        return ResponseEntity.ok(shopRepository.getAreaExcluded(excludedArea.orElse(-1), minDiscount.orElse(Double.MIN_VALUE), maxDiscount.orElse(Double.MAX_VALUE)));
    }

    private Shop applyPatchToArea(JsonPatch patch, Shop shop) throws JsonPatchException, JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();

        JsonNode patched = patch.apply(objectMapper.convertValue(shop, JsonNode.class));
        return objectMapper.treeToValue(patched, Shop.class);
    }

}
