package ru.pavlovka.homework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.entity.Shop;

import java.util.Collection;

public interface ShopRepository extends JpaRepository<Shop, Integer> {
    @Query(value = "SELECT * FROM shop WHERE area_id!=:excludedArea AND shop.discount BETWEEN :minDiscount AND :maxDiscount", nativeQuery=true)
    Collection<Shop> getAreaExcluded(@Param("excludedArea") int excludedAreA, @Param("minDiscount") double minDiscount, @Param("maxDiscount") double maxDiscount);
}
