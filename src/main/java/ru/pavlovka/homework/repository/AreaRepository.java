package ru.pavlovka.homework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.responseData.CustomersOnlyNameAndDiscount;

import java.util.Collection;

public interface AreaRepository extends JpaRepository<Area, Integer> {
    @Query(value = "SELECT area.id as id, area.name as name FROM area WHERE area.id IN (SELECT DISTINCT customer.area_id FROM customer)", nativeQuery=true)
    Collection<Area> customerAreas();

    @Query(value = "SELECT customer.name, customer.discount FROM customer WHERE area_id=:area", nativeQuery=true)
    Collection<CustomersOnlyNameAndDiscount> getCustomersByArea(@Param("area") int areaId);

    @Query(value = "SELECT DISTINCT shop.name FROM shop WHERE area_id=:area", nativeQuery=true)
    Collection<String> getShopsByArea(@Param("area") int areaId);
}
