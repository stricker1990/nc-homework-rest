package ru.pavlovka.homework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.pavlovka.homework.entity.Area;
import ru.pavlovka.homework.entity.Customer;

import java.util.Collection;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "SELECT area.id as id, area.name as name FROM area WHERE area.id IN (SELECT DISTINCT customer.area_id FROM customer)", nativeQuery=true)
    Collection<Area> getAllAreas();
}
