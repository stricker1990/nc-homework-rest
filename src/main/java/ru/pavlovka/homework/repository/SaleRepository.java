package ru.pavlovka.homework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pavlovka.homework.entity.Sale;
import ru.pavlovka.homework.responseData.BookSales;

import java.util.Collection;
import java.util.Date;

public interface SaleRepository extends JpaRepository<Sale, Integer> {
    @Query(value = "SELECT DISTINCT date_trunc('month', sale.date) AS month FROM sale", nativeQuery=true)
    Collection<Date> getSaleMonths();

    @Query(value = "SELECT * FROM sale WHERE sale.sum>=:minSum", nativeQuery=true)
    Collection<Sale> getAllBy(@Param("minSum") double minSum);

    @Query(value = "SELECT * FROM sale WHERE sale.customer=:customerId AND sale.date>=:startDate AND sale.seller IN(SELECT shop.id FROM shop WHERE shop.area_id=:areaId)", nativeQuery=true)
    Collection<Sale> getAllBySellerAndArea(@Param("customerId") int customerId, @Param("startDate") Date startDate, @Param("areaId") int areaId);

    @Query(value = "SELECT book.name as book, :areaName as areaname, sale.sum as saleSum, book.count AS bookcount  FROM sale INNER JOIN book ON sale.seller=book.id AND sale.seller IN(SELECT shop.id FROM shop WHERE area_id=:areaId) AND book.count>:minCount", nativeQuery = true)
    Collection<BookSales> getBookSales(@Param("areaName") String areaName, @Param("areaId") int areaid, @Param("minCount") int minCount);
}
