package ru.pavlovka.homework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pavlovka.homework.entity.Book;
import ru.pavlovka.homework.responseData.UniqueBookName;

import java.util.Collection;

public interface BookRepository extends JpaRepository<Book, Integer> {

    @Query(value = "SELECT DISTINCT book.name as name, book.cost as cost FROM book", nativeQuery=true)
    Collection<UniqueBookName> getUniqueNames();

    @Query(value = "SELECT * FROM book WHERE book.name LIKE %:keyword% OR book.cost>=:minCost ORDER BY book.name, book.cost DESC", nativeQuery=true)
    Collection<Book> getAllBy(@Param("keyword") String keyword, @Param("minCost") double minCost);
}
