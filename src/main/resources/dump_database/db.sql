PGDMP         (                x            nc_rest    12.2    12.2 #    5           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            6           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            7           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            8           1262    16474    nc_rest    DATABASE     �   CREATE DATABASE nc_rest WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE nc_rest;
                postgres    false            �            1259    16487    area    TABLE     R   CREATE TABLE public.area (
    id integer NOT NULL,
    name character varying
);
    DROP TABLE public.area;
       public         heap    postgres    false            �            1259    16485    areas_id_seq    SEQUENCE     �   ALTER TABLE public.area ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    205            �            1259    16510    book    TABLE     �   CREATE TABLE public.book (
    id integer NOT NULL,
    name character varying,
    cost double precision,
    shop_id integer,
    count integer
);
    DROP TABLE public.book;
       public         heap    postgres    false            �            1259    16555    book_id_seq    SEQUENCE     �   ALTER TABLE public.book ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    207            �            1259    16475    customer    TABLE     �   CREATE TABLE public.customer (
    id integer NOT NULL,
    discount double precision,
    name character varying,
    area_id integer
);
    DROP TABLE public.customer;
       public         heap    postgres    false            �            1259    16483    customer_id_seq    SEQUENCE     �   ALTER TABLE public.customer ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    202            �            1259    16520    sale    TABLE     �   CREATE TABLE public.sale (
    id integer NOT NULL,
    date date,
    seller integer,
    customer integer,
    book integer,
    count integer,
    sum double precision
);
    DROP TABLE public.sale;
       public         heap    postgres    false            �            1259    16553    sale_id_seq    SEQUENCE     �   ALTER TABLE public.sale ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    208            �            1259    16497    shop    TABLE     �   CREATE TABLE public.shop (
    id integer NOT NULL,
    name character varying,
    area_id integer,
    discount double precision
);
    DROP TABLE public.shop;
       public         heap    postgres    false            �            1259    16551    shop_id_seq    SEQUENCE     �   ALTER TABLE public.shop ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.shop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    206            ,          0    16487    area 
   TABLE DATA           (   COPY public.area (id, name) FROM stdin;
    public          postgres    false    205   L%       .          0    16510    book 
   TABLE DATA           >   COPY public.book (id, name, cost, shop_id, count) FROM stdin;
    public          postgres    false    207   �%       )          0    16475    customer 
   TABLE DATA           ?   COPY public.customer (id, discount, name, area_id) FROM stdin;
    public          postgres    false    202   �%       /          0    16520    sale 
   TABLE DATA           L   COPY public.sale (id, date, seller, customer, book, count, sum) FROM stdin;
    public          postgres    false    208   �%       -          0    16497    shop 
   TABLE DATA           ;   COPY public.shop (id, name, area_id, discount) FROM stdin;
    public          postgres    false    206   +&       9           0    0    areas_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.areas_id_seq', 2, true);
          public          postgres    false    204            :           0    0    book_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.book_id_seq', 1, true);
          public          postgres    false    211            ;           0    0    customer_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.customer_id_seq', 1, true);
          public          postgres    false    203            <           0    0    sale_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.sale_id_seq', 2, true);
          public          postgres    false    210            =           0    0    shop_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.shop_id_seq', 2, true);
          public          postgres    false    209            �
           2606    16491    area areas_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.area
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);
 9   ALTER TABLE ONLY public.area DROP CONSTRAINT areas_pkey;
       public            postgres    false    205            �
           2606    16514    book book_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_pkey;
       public            postgres    false    207            �
           2606    16482    customer customer_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_pkey;
       public            postgres    false    202            �
           2606    16524    sale sale_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.sale
    ADD CONSTRAINT sale_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.sale DROP CONSTRAINT sale_pkey;
       public            postgres    false    208            �
           2606    16504    shop shop_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.shop DROP CONSTRAINT shop_pkey;
       public            postgres    false    206            �
           2606    16492    customer area_fk    FK CONSTRAINT     x   ALTER TABLE ONLY public.customer
    ADD CONSTRAINT area_fk FOREIGN KEY (area_id) REFERENCES public.area(id) NOT VALID;
 :   ALTER TABLE ONLY public.customer DROP CONSTRAINT area_fk;
       public          postgres    false    205    2718    202            �
           2606    16515    book book_shop_id_fkey    FK CONSTRAINT     t   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_shop_id_fkey FOREIGN KEY (shop_id) REFERENCES public.shop(id);
 @   ALTER TABLE ONLY public.book DROP CONSTRAINT book_shop_id_fkey;
       public          postgres    false    2720    207    206            �
           2606    16535    sale sale_book_fkey    FK CONSTRAINT     n   ALTER TABLE ONLY public.sale
    ADD CONSTRAINT sale_book_fkey FOREIGN KEY (book) REFERENCES public.book(id);
 =   ALTER TABLE ONLY public.sale DROP CONSTRAINT sale_book_fkey;
       public          postgres    false    207    2722    208            �
           2606    16530    sale sale_customer_fkey    FK CONSTRAINT     z   ALTER TABLE ONLY public.sale
    ADD CONSTRAINT sale_customer_fkey FOREIGN KEY (customer) REFERENCES public.customer(id);
 A   ALTER TABLE ONLY public.sale DROP CONSTRAINT sale_customer_fkey;
       public          postgres    false    2716    208    202            �
           2606    16525    sale sale_seller_fkey    FK CONSTRAINT     r   ALTER TABLE ONLY public.sale
    ADD CONSTRAINT sale_seller_fkey FOREIGN KEY (seller) REFERENCES public.shop(id);
 ?   ALTER TABLE ONLY public.sale DROP CONSTRAINT sale_seller_fkey;
       public          postgres    false    208    2720    206            �
           2606    16505    shop shop_area_id_fkey    FK CONSTRAINT     t   ALTER TABLE ONLY public.shop
    ADD CONSTRAINT shop_area_id_fkey FOREIGN KEY (area_id) REFERENCES public.area(id);
 @   ALTER TABLE ONLY public.shop DROP CONSTRAINT shop_area_id_fkey;
       public          postgres    false    2718    206    205            ,   +   x�3�0��.l���¾��]�r���.��N�=... �[N      .   3   x�3�0���6(\���^l��ta߅�@��������Hq��qqq T�j      )   "   x�3�44�0�¦.콰��&NC�=... �(
�      /      x�3�4202�50�50�C�=... M�      -   *   x�3�0���x���*qrp��B�c���� �>3     